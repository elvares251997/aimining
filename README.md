# Process Mining DataBase

## Общие ссылки
* [ОПЫТ ПРИМЕНЕНИЯ PROCESS MINING ДЛЯ ЦЕЛЕЙ АУДИТА](https://newtechaudit.ru/process-mining-dlya-czelej-audita/)
* [Hierarchical Process Mining](https://www.minit.io/hierarchical-process-mining-and-process-simulation#accept)
* [Process Mining and Simulation](https://www.fluxicon.com/blog/2013/10/combining-process-mining-and-simulation/)
* [What if simulation](https://docviewer.yandex.ru/view/148333598/?*=1nv%2BfCib46sIta9XC0uFQ%2B5dpJF7InVybCI6Imh0dHA6Ly93d3ctZGIuZGVpcy51bmliby5pdC9%2Bc3JpenppL1BERi9pamR3bTA5LVVNTC5wZGYiLCJ0aXRsZSI6ImlqZHdtMDktVU1MLnBkZiIsIm5vaWZyYW1lIjp0cnVlLCJ1aWQiOiIxNDgzMzM1OTgiLCJ0cyI6MTU4NzM3NDM3MjUxNywieXUiOiIzMDU2MjkwNzYxNTY4MTEwNzEwIiwic2VycFBhcmFtcyI6Imxhbmc9ZW4mdG09MTU4NjI4MDAwMCZ0bGQ9cnUmbmFtZT1pamR3bTA5LVVNTC5wZGYmdGV4dD1tb2RlbGluZyt3aGF0K2lmK3N5c3RlbXMmdXJsPWh0dHAlM0EvL3d3dy1kYi5kZWlzLnVuaWJvLml0L35zcml6emkvUERGL2lqZHdtMDktVU1MLnBkZiZscj0yMTMmbWltZT1wZGYmbDEwbj1ydSZ0eXBlPXRvdWNoJnNpZ249NzFhMjE0N2QyMWRmNjJkZDlkMjA4N2Y1NTJjOTc5ODkma2V5bm89MCJ9&lang=en)

## GitHub проекты
* [The Numenta Anomaly Benchmark](https://github.com/numenta/NAB)
* [NetGraph - визуализация графов](https://github.com/paulbrodersen/netgraph)
* [PM4Knime](https://github.com/pm4knime/pm4knime-document/wiki)

## Датасеты
* [Датасеты для тестирования поиска аномалий](http://odds.cs.stonybrook.edu/)

## Статьи
### Поиск аномалий

### Суммаризация
* [Техники extractive суммаризации текстов](https://arxiv.org/pdf/1707.02268v3.pdf)
* [ROUGE метрики](https://www.aclweb.org/anthology/D18-1443.pdf)

### Поиск инсайтов